pComplexIT
==========

(Semantic) complexity measures for agent societies (e.g. promise network models of human-machine systems).
